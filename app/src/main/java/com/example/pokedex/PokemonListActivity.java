package com.example.pokedex;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pokedex.adapter.AdapterForPokemons;
import com.example.pokedex.model.Pokemon;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PokemonListActivity extends AppCompatActivity {

    private static final String TAG = PokemonListActivity.class.getSimpleName();

    public static final String INPUT_PARAMETER = "input_parameter";

    public static final int RESULT_OK = 0;
    public static final int RESULT_KO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        // Create global configuration and initialize ImageLoader with this config
        /*ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);*/

        List<Pokemon> listPokemons = new ArrayList<>(0);

        RequestQueue queue = Volley.newRequestQueue(this);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
        {
            Log.d(TAG, "Connected to internet");

            String url = "https://pokeapi.co/api/v2/pokemon?limit=151";

            Log.d(TAG, "URL is " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            Log.d(TAG, "IUT Response" + response);

                            try {
                                JSONObject json = new JSONObject(response);
                                JSONArray listOfPokemons = json.getJSONArray("results");

                                JSONObject jsonItem;
                                Pokemon pokemon;
                                for (int i=0; i<listOfPokemons.length(); i++)
                                {
                                    jsonItem = listOfPokemons.getJSONObject(i);
                                    System.out.println(jsonItem);

                                    pokemon = new Pokemon(jsonItem.getString("name"), jsonItem.getString("url"));
                                    listPokemons.add(pokemon);
                                }

                                ListView listView = (ListView) findViewById(R.id.listViewPokemon);
                                AdapterForPokemons adapterForPokemons = new AdapterForPokemons(PokemonListActivity.this, listPokemons);
                                listView.setAdapter(adapterForPokemons);
                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Log.d(TAG, listPokemons.get(position).getName());
                                    }
                                });
                            } catch (JSONException e) {
                                Log.e(TAG, "Error while parsing games result", e);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Erreur de requête", Toast.LENGTH_LONG);
                    toast.show();
                }
            });
            queue.add(stringRequest);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(), "Non connecté à internet", Toast.LENGTH_LONG);
            toast.show();
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_KO);
        finish();
    }
}
