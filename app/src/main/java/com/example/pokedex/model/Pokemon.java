package com.example.pokedex.model;

public class Pokemon {

    private String id;
    private String name;
    private String urlDetails;
    private String urlPhoto;

    public Pokemon(String name, String url) {
        this.name = name;
        this.urlDetails = url;
        String[] urlSplit = url.split("/");
        this.id = urlSplit[urlSplit.length - 1];
        this.urlPhoto = "https://pokeres.bastionbot.org/images/pokemon/"+urlSplit[urlSplit.length - 1]+".png";
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public String getName(){
        return name;
    }

}
