package com.example.pokedex.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pokedex.R;
import com.example.pokedex.model.Pokemon;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterForPokemons extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Pokemon> pokemons;

    public AdapterForPokemons(Context context, List<Pokemon> pokemons) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pokemons = pokemons;
    }

    @Override
    public int getCount() {
        return this.pokemons.size();
    }

    @Override
    public Object getItem(int position) {
        return this.pokemons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = this.layoutInflater.inflate(R.layout.game_list_item, parent, false);

        ImageView pokemonImage = (ImageView) itemView.findViewById(R.id.pokemon_image);
        TextView pokemonName = (TextView) itemView.findViewById(R.id.pokemon_title);
        //TextView pokemonDescription = (TextView) itemView.findViewById(R.id.pokemon_description);

        //ImageLoader.getInstance().displayImage(this.games.get(position).getThumbnail(), gameImage);

        Picasso.get().load(this.pokemons.get(position).getUrlPhoto()).into(pokemonImage);
        pokemonName.setText(this.pokemons.get(position).getName());
        //gameDescription.setText(this.games.get(position).getShortDescription());

        return itemView;
    }
}
